import React from 'react';
import './style.scss';

export default function TextLoader({ size }) {
    return (
        <div style={{ fontSize: size }} className="text-loader"> a </div>
    )
}