import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss'

import ProductItem from '../product-item';

export default function ProductList(props) {

    return (
        <div className="product-list">
            {
                props.items.map(({id,...otherProps}) => {
                    return (
                        <Link key={id} className="a--nostyle" to={`/items/${id}`}>
                            <ProductItem
                                id={id}
                                {...otherProps}>
                            </ProductItem>
                        </Link>
                    )
                })
            }
        </div>
    )
}