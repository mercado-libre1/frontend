import React from 'react';
import LogoML from '../../assets/Logo_ML.png';
import SearchIcon from '../../assets/ic_Search.png';
import { Link } from 'react-router-dom';
import './style.scss'

export default function SearchBox(props) {
    const query = decodeURI(window.location.search.split("=")[1] || "");
    const [input, setInput] = React.useState(query);

    const inputChanged = (event) => {
        setInput(event.target.value);
    }

    const onSearch = (event) => {
        if ((event || {}).keyCode === 13 || !(event || {}).keyCode) {
            props.doSearch(input);
        }
    }

    return (
        <div className="search-box">
            <div className="search--content">
                <Link to="/">
                    <img src={LogoML} alt="logo" className="logo-ml" />
                </Link>
                <div className="input-group">
                    <input value={input} onChange={inputChanged} onKeyDown={onSearch} type="text" placeholder="Nunca dejes de Buscar" />
                    <button onClick={onSearch}>
                        <img src={SearchIcon} alt="search" />
                    </button>
                </div>
            </div>
        </div >
    )
}