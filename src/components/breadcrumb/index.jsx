import React from 'react';
import './style.scss';

export default function BreadCrumb(props) {

    return (
        <div className="breadcrumb">
            {props.categories.map(c => <span className="category" key={c}>{c}</span>)}
        </div>
    )
}