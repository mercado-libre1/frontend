import React from 'react';
import FreeShippingIcon from '../../assets/ic_shipping.png';
import './style.scss';

export default function ProductItem(props) {
    const formatPrice = Intl.NumberFormat().format(props.price.amount);

    return (
        <div className="product-item" >
            <img className="item--pic" src={props.picture} alt="product" />
            <div className="item--description">
                <div className="item--title">{props.title}</div>
                <div className="item--price">
                    {/* <span className="item--currency">{props.price.currency}</span> */}
                    <span className="item--amount">${formatPrice}</span >
                    {props.free_shipping && <img src={FreeShippingIcon} alt="free shipping" />}
                </div>
            </div>
            <div className="item--condition">
                {props.address}
            </div>
        </div>
    )
}