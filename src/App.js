import React, { Component } from 'react';
import {
  Switch,
  Route,
  withRouter
} from "react-router-dom";
import './App.css';

import ItemsPage from './pages/items';
import ItemDetailsPage from './pages/item-details';
import SearchBox from './components/searchbox';
import NoSearch from './components/nosearch';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      url: ''
    }
  }

  searchItems = (input) => {
    this.props.history.push(`/items?search=${encodeURI(input)}`);
  }

  render() {
    return (
      <div className="App" >
        <header>
          <SearchBox doSearch={this.searchItems}></SearchBox>
        </header>

        <main>

          {/* {this.state.redirect === true && <Redirect to={this.state.url} />} */}
          <Switch>
            <Route exact path="/">
              <NoSearch />
            </Route>
            <Route path="/items/:id" component={ItemDetailsPage} />
            <Route path="/items" component={ItemsPage} />
          </Switch>
        </main>

      </div>
    )
  }
}

export default withRouter(App);
