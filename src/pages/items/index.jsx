import React, { Component } from 'react';

import BreadCrumb from '../../components/breadcrumb';
import ProductList from '../../components/product-list';
import LoadingItems from '../../components/loading-items';
import NoItems from '../../components/noitems';

export default class Items extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            categories: [],
            loading: false
        }
    }

    componentDidMount() {
        const { search } = this.props.location;
        if (search) {
            const query = search.split("=")[1];
            if (query) {
                this.getItems(decodeURI(query));
            }
        }
    }

    componentDidUpdate(prevProps) {
        const prevQuery = prevProps.location.search.split("=")[1];
        const query = this.props.location.search.split("=")[1];
        if (prevQuery !== query) {
            this.getItems(query);
        }
    }

    getItems = async (query) => {
        console.debug(`we're looking for a "${query}" item`);

        if (!query) {
            return;
        }

        try {
            this.setState({ loading: true });
            const r = await fetch(`http://localhost:3001/api/items?q=${query}`);
            const data = await r.json();

            this.setState({
                items: data.items,
                categories: data.categories,
                loading: false
            });
        } catch (e) {
            console.debug(`something went wrong: ${e}`);
        }
    }

    render() {
        const { categories = [], items = [], loading } = this.state;
        let content;
        if (items.length) content = <ProductList items={items}></ProductList>;
        else if (loading) content = <LoadingItems />;
        else content = <NoItems />

        return (
            <div id="items-page" className="page">
                <BreadCrumb categories={categories} />
                {content}
            </div>

        )
    }
}