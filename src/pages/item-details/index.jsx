import React, { Component } from 'react';
import BreadCrumb from '../../components/breadcrumb';
import TextLoader from '../../components/text-loader';
import NoProductImage from '../../assets/680x450.png';
import './style.scss'

export default class ItemDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            categories: [],
            item: {},
            loading: true
        }
    }

    componentDidMount() {
        const { id } = this.props.match.params;
        this.getItemDetails(id);
    }

    getItemDetails = async (id) => {
        console.debug(`getting item data for id ${id}`);

        try {
            const r = await fetch(`http://localhost:3001/api/items/${id}`);
            const data = await r.json();

            this.setState({
                item: data.item,
                categories: data.categories,
                loading: false
            });
        } catch (e) {
            console.debug(`something went wrong: ${e}`);
        }
    }

    render() {
        const { loading, item } = this.state;
        const formatPrice = Intl.NumberFormat().format(((item || {}).price || {}).amount || "");

        return (
            <div id="item-details-page" className="page">
                <BreadCrumb categories={this.state.categories} />

                <div className="item--profile">
                    <div className="item--details">
                        <div className="item--pic">
                            <img alt="product" src={(item || {}).picture || NoProductImage} />
                        </div>

                        <div className="item--description">
                            <span className="description--title">Descripción del producto</span>
                            <span className="description--content">{(item || {}).description || ""}</span>
                        </div>

                    </div>
                    <div className="item--buy">
                        <div className="status">{loading ? <TextLoader size={14} /> : `${(item || {}).condition} - ${(item || {}).sold_quantity} vendidos`}</div>
                        <div className="title"> {loading ? <TextLoader size={24} /> : ((item || {}).title || "")}</div>
                        <div className="price"> {loading ? <TextLoader size={46} /> : `$ ${formatPrice}`}</div>
                        <button className="buy">Comprar</button>
                    </div>
                </div>

            </div>
        )
    }
}